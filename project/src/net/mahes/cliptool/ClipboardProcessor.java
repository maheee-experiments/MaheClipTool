package net.mahes.cliptool;

public interface ClipboardProcessor {

	public ClipboardProcessorResponse process(String lastValue, String currentValue);
}
