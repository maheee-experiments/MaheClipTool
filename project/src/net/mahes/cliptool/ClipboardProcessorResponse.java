package net.mahes.cliptool;

public class ClipboardProcessorResponse {

	static public enum ClipboardProcessorResponseType {
		INTERNAL_ERROR,
		DID_NOT_FIT,
		PROCESSED_NO_CHANGE,
		PROCESSED_STOP,
		PROCESSED_CONTINUE_WITH_OLD_VALUE,
		PROCESSED_CONTINUE_WITH_NEW_VALUE,
	}

	private final ClipboardProcessorResponseType type;
	private final String value;

	public ClipboardProcessorResponse(ClipboardProcessorResponseType type) {
		this(type, null);
	}

	public ClipboardProcessorResponse(ClipboardProcessorResponseType type, String value) {
		this.type = type;
		this.value = value;
	}

	public ClipboardProcessorResponseType getType() {
		return type;
	}

	public String getValue() {
		return value;
	}

}
