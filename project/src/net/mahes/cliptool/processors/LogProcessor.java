package net.mahes.cliptool.processors;

import java.util.logging.Logger;

import net.mahes.cliptool.ClipboardProcessor;
import net.mahes.cliptool.ClipboardProcessorResponse;
import net.mahes.cliptool.ClipboardProcessorResponse.ClipboardProcessorResponseType;

public class LogProcessor implements ClipboardProcessor {

	private static Logger logger = Logger.getLogger(LogProcessor.class.getName());
	
	@Override
	public ClipboardProcessorResponse process(String lastValue, String currentValue) {
		logger.fine(currentValue);
		return new ClipboardProcessorResponse(ClipboardProcessorResponseType.PROCESSED_NO_CHANGE);
	}

}
