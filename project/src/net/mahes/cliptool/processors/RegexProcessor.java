package net.mahes.cliptool.processors;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.mahes.cliptool.ClipboardProcessor;
import net.mahes.cliptool.ClipboardProcessorResponse;
import net.mahes.cliptool.ClipboardProcessorResponse.ClipboardProcessorResponseType;

public class RegexProcessor implements ClipboardProcessor {

	private Pattern pattern;
	private String replacement;
	
	public RegexProcessor(String pattern, String replacement) {
		this.pattern = Pattern.compile(pattern);
		this.replacement = replacement;
	}
	
	@Override
	public ClipboardProcessorResponse process(String lastValue, String currentValue) {
		
		Matcher matcher = pattern.matcher(currentValue);
		
		if (!matcher.matches()) {
			return new ClipboardProcessorResponse(ClipboardProcessorResponseType.DID_NOT_FIT);			
		}
		
		String result = matcher.replaceAll(replacement);
		
		return new ClipboardProcessorResponse(ClipboardProcessorResponseType.PROCESSED_CONTINUE_WITH_NEW_VALUE, result);
	}

}
