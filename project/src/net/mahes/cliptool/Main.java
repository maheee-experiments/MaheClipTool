package net.mahes.cliptool;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;

import net.mahes.cliptool.processors.LogProcessor;
import net.mahes.cliptool.processors.RegexProcessor;

public class Main {

	public static void main(String... args) {
		// LogManager logManager = LogManager.getLogManager();

		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		ClipboardManager cm = new ClipboardManager(clipboard);

		cm.addClipboardProcessor(new LogProcessor());
		cm.addClipboardProcessor(new RegexProcessor("(.*)([0-9])(.*)", "$1_$3"));
		cm.addClipboardProcessor(new LogProcessor());

		cm.start();
	}
}
