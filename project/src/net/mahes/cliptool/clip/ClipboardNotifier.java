package net.mahes.cliptool.clip;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClipboardNotifier implements Runnable {

	private static Logger logger = Logger.getLogger(ClipboardNotifier.class.getName());

	private Thread workingThread;
	private boolean enabled;
	private List<ClipboardListener> listenerList;
	private Transferable oldValue;
	private long frequency = 5000;
	private Clipboard clipboard;

	/**
	 * Construct a new <code>ClipboardNotifier</code>
	 */
	public ClipboardNotifier(Clipboard clipboard) {
		this.listenerList = new ArrayList<ClipboardListener>();
		this.clipboard = clipboard;
		this.oldValue = clipboard.getContents(null);
	}

	/**
	 * Adds an <code>ClipboardListener</code> to the notifier.
	 * 
	 * @param listener
	 *            the <code>ClipboardListener</code> to be added
	 */
	public void addClipboardListener(ClipboardListener listener) {
		listenerList.add(listener);
	}

	/**
	 * Getter
	 * 
	 * @return delay between checks
	 */
	public long getFrequency() {
		return frequency;
	}

	/**
	 * Getter
	 * 
	 * @return <code>true</code> if this notifier is enabled, <code>false</code>
	 *         otherwise
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Removes an <code>ClipboardListener</code> from the notifier
	 * 
	 * @param listener
	 */
	public void removeClipboardListener(ClipboardListener listener) {
		listenerList.remove(listener);
	}

	/**
	 * Check if the new and old Value are equal
	 * 
	 * @param oldValue
	 *            Old value of clipboard
	 * @param newValue
	 *            New value of clipboard
	 * @param flavor
	 *            <code>DataFlavor</code> of the new value for which the test
	 *            should be done
	 * @return <code>true</code> is the both cliboard values are equals,
	 *         <code>false</code> is the values are not equals or the old value
	 *         don't support this <code>DataFlavor</code>
	 */
	private boolean checkEquality(Transferable oldValue, Transferable newValue,
			DataFlavor flavor) {

		if (oldValue.isDataFlavorSupported(flavor)
				&& newValue.isDataFlavorSupported(flavor)) {
			try {
				return oldValue.getTransferData(flavor).equals(
						newValue.getTransferData(flavor));
			} catch (UnsupportedFlavorException e) {
				logger.log(Level.WARNING, "Exception occured!", e);
			} catch (IOException e) {
				logger.log(Level.WARNING, "Exception occured!", e);
			}
		}
		return false;
	}

	@Override
	public void run() {
		while (enabled) {
			try {
				Thread.sleep(frequency);
			} catch (InterruptedException e) {
			}

			Transferable newValue;
			try {
				newValue = clipboard.getContents(null);
			} catch (Exception e) {
				logger.log(Level.WARNING, "Exception occured!", e);
				continue;
			}
			for (ClipboardListener listener : listenerList) {
				if (newValue.isDataFlavorSupported(listener.getDataFlavor())) {
					if (!checkEquality(oldValue, newValue,
							listener.getDataFlavor())) {
						try {
							listener.valueChanged(
									oldValue.isDataFlavorSupported(listener
											.getDataFlavor()) ? oldValue
											.getTransferData(listener
													.getDataFlavor()) : null,
									newValue.getTransferData(listener
											.getDataFlavor()));
						} catch (UnsupportedFlavorException e) {
							logger.log(Level.WARNING, "Exception occured!", e);
						} catch (IOException e) {
							logger.log(Level.WARNING, "Exception occured!", e);
						}
					}
				}
			}
			oldValue = newValue;
		}
	}

	/**
	 * Enable (or disable) the notifier
	 * 
	 * @param enabled
	 *            true to enable the notifier, otherwise false
	 */
	public void setEnabled(boolean enabled) {
		if (this.enabled != enabled) {
			this.enabled = enabled;
			if (enabled) {
				workingThread = new Thread(this);
				workingThread.start();
			}
		}
	}

	/**
	 * Setter
	 * 
	 * @param frequency
	 *            delay between checks
	 */
	public void setFrequency(long frequency) {
		this.frequency = frequency;
	}

}