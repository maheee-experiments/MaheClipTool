package net.mahes.cliptool.clip;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;

public class ClipboardSetter<T> {

	private Clipboard clipboard;
	private ClipboardOwner clipboardOwner;
	
	public ClipboardSetter(Class<T> type, Clipboard clipboard, ClipboardOwner clipboardOwner) {
		this.clipboard = clipboard;
		this.clipboardOwner = clipboardOwner;
	}
	
	public void set(T value) {
		Transferable transferable = new TransferableValue<String>(String.class, value);
		clipboard.setContents(transferable, clipboardOwner);
	}
}
