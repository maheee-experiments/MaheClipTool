package net.mahes.cliptool.clip;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class TransferableValue<T> implements Transferable {

	private final Object value;
	private final DataFlavor flavor;

	public TransferableValue(Class<T> type, Object value) {
		flavor = new DataFlavor(type, type.getCanonicalName());
		this.value = value;
	}

	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
		if (!isDataFlavorSupported(flavor))
			throw new UnsupportedFlavorException(flavor);
		return value;
	}

	@Override
	public DataFlavor[] getTransferDataFlavors() {
		return new DataFlavor[] { flavor };
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		return this.flavor == flavor;
	}

}