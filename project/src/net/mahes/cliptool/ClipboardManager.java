package net.mahes.cliptool;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.util.LinkedList;
import java.util.logging.Logger;

import net.mahes.cliptool.ClipboardProcessorResponse.ClipboardProcessorResponseType;
import net.mahes.cliptool.clip.ClipboardListener;
import net.mahes.cliptool.clip.ClipboardNotifier;
import net.mahes.cliptool.clip.ClipboardSetter;

public class ClipboardManager extends ClipboardListener<String> implements ClipboardOwner {

	private static Logger logger = Logger.getLogger(ClipboardManager.class.getName());
	
	public ClipboardManager(Clipboard clipboard) {
		super(String.class);
		
		this.clipboard = clipboard;
		this.clipboardSetter = new ClipboardSetter<String>(String.class, clipboard, this);
		
		this.processors = new LinkedList<ClipboardProcessor>();
	}
	
	/*
	 * 
	 */
	
	private Clipboard clipboard;
	private ClipboardNotifier clipboardNotifier = null;
	private ClipboardSetter<String> clipboardSetter;

	private boolean amOwner = false;

	public void start() {
		stop();
		clipboardNotifier = new ClipboardNotifier(clipboard);
		clipboardNotifier.addClipboardListener(this);
		clipboardNotifier.setFrequency(10);
		clipboardNotifier.setEnabled(true);
	}

	public void stop() {
		if (clipboardNotifier != null) {
			clipboardNotifier.setEnabled(false);
		}
		clipboardNotifier = null;
	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable transferable) {
		amOwner = false;
	}

	@Override
	public void valueChanged(String oldValue, String newValue) {
		if (amOwner) {
			return;
		}
		process(oldValue, newValue);
	}

	/*
	 * 
	 */

	private LinkedList<ClipboardProcessor> processors;
	
	public void addClipboardProcessor(ClipboardProcessor processor) {
		processors.addLast(processor);
	}
	
	public void addClipboardProcessor(int index, ClipboardProcessor processor) {
		processors.add(index, processor);
	}
	
	public void removeClipboardProcessor(ClipboardProcessor processor) {
		processors.remove(processor);
	}
	
	public void removeClipboardProcessor(int index) {
		processors.remove(index);
	}

	private void process(String oldValue, String newValue) {
		for (ClipboardProcessor processor : processors) {
			ClipboardProcessorResponse response = processor.process(oldValue, newValue);
			ClipboardProcessorResponseType type = response.getType();
			String value = response.getValue();

			logger.info(processor.getClass().getSimpleName() + ": " + type + ": " + value);
			
			switch (type) {
			case INTERNAL_ERROR:
				throw new RuntimeException("");
			case DID_NOT_FIT:
				break;
			case PROCESSED_NO_CHANGE:
				break;
			case PROCESSED_STOP:
				clipboardSetter.set(value);
				amOwner = true;
				return;
			case PROCESSED_CONTINUE_WITH_OLD_VALUE:
				clipboardSetter.set(value);
				amOwner = true;
				break;
			case PROCESSED_CONTINUE_WITH_NEW_VALUE:
				newValue = value;
				clipboardSetter.set(value);
				amOwner = true;
				break;
			}
		}
	}

}
